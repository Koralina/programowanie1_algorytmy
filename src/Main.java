public class Main {


    static void bubbleSort(int[] table) {
        int n = table.length;
        int temp = 0;

        for(int i = 0; i < n; i++) {
            for(int j=1; j < (n-i); j++) {
                if(table[j-1] > table[j]) {
                    temp = table[j-1];
                    table[j-1] = table [j];
                    table[j] = temp;
                }
            }
        }
    }
    public static void main(String[] args) {
        int arr[] = { 8,4,2,7,5,3,1 };
        System.out.println("Array Before Bubble Sort");

        for(int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
        bubbleSort(arr);
        System.out.println("Array After Bubble Sort");

        for(int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}
